<?php

Route::get('/', 'HomeController@index');
Route::get('about', 'HomeController@about');
Route::get('carrier', 'HomeController@carrier');
Route::get('galeri/{album_id?}', 'HomeController@gallery');
Route::get('contact', 'HomeController@contact');
Route::resource('kontak', 'ContactController');

Auth::routes();

Route::group(['prefix'=>'admin', 'middleware'=>['auth', 'role:admin']], function(){
  Route::get('dashboard', function(){
    return view('dashboard')->with('page_title', 'Halaman dashboard Admin ');
  });
  Route::resource('kategori', 'CategoriController');
  Route::resource('post', 'PostController');
  Route::resource('statis', 'StatisController');
  Route::resource('slideshow', 'SlideshowController');
  Route::resource('album', 'AlbumController');
  Route::resource('gallery', 'GalleryController');
  Route::resource('download', 'DownloadController');
  Route::get('file/download/{id}', 'DownloadController@download');
  Route::resource('user', 'UserController');
  Route::resource('identitas', 'IdentitasController');


  Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
});
Route::get('/home', 'HomeController@index');

View::composer('frontend', function($view){
  $view->identitas = App\Identitas::where('id',1)->first();
});
