<?php

return [
    'use_package_routes' => true,
    'middlewares' => ['web', 'auth'],
    'rename_file'           => true,
    // true : files will be renamed as uniqid
    // false : files will remain original names

    // true : filter filename characters which are not alphanumeric, and replace them with '_'
    'alphanumeric_filename' => true,

    'allow_multi_user'      => true,
    // true : user can upload files to shared folder and their own folder
    // false : all files are put together in shared folder

    'user_field'            => 'id',
    // determine which column of users table will be used as user's folder name

    // valid image mimetypes
    'valid_image_mimetypes' => [
        'image/jpeg',
        'image/pjpeg',
        'image/png',
        'image/gif'
    ],


    // valid file mimetypes (only when '/laravel-filemanager?type=Files')
    'valid_file_mimetypes' => [
        'image/jpeg',
        'image/pjpeg',
        'image/png',
        'image/gif',
        'application/pdf',
        'text/plain'
    ],

    'images_dir'         => 'public/vendor/laravel-filemanager/images/',
    'images_url'         => '/vendor/laravel-filemanager/images/',

    'files_dir'          => 'public/vendor/laravel-filemanager/files/',
    'files_url'          => '/vendor/laravel-filemanager/files/',

    'allowed_file_types'    => [
        // Allowed file types.
        // Leave empty to authorize all types.
        "Files" => [],
        // Allowed image types.
        // Leave empty to authorize all types.
        "Images" => ["jpg", "jpeg", "png", "gif"]
    ],

    'file_type_array'         => [
        "pdf"  => "Adobe Acrobat",
        "docx" => "Microsoft Word",
        "docx" => "Microsoft Word",
        "xls"  => "Microsoft Excel",
        "xls"  => "Microsoft Excel",
        "zip"  => 'Archive',
        "gif"  => 'GIF Image',
        "jpg"  => 'JPEG Image',
        "jpeg" => 'JPEG Image',
        "png"  => 'PNG Image',
        "ppt"  => 'Microsoft PowerPoint',
        "pptx" => 'Microsoft PowerPoint',
    ],

    'file_icon_array'         => [
        "pdf"  => "fa-file-pdf-o",
        "docx" => "fa-file-word-o",
        "docx" => "fa-file-word-o",
        "xls"  => "fa-file-excel-o",
        "xls"  => "fa-file-excel-o",
        "zip"  => 'fa-file-archive-o',
        "gif"  => 'fa-file-image-o',
        "jpg"  => 'fa-file-image-o',
        "jpeg" => 'fa-file-image-o',
        "png"  => 'fa-file-image-o',
        "ppt"  => 'fa-file-powerpoint-o',
        "pptx" => 'fa-file-powerpoint-o',
    ],
];
