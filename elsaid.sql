-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 05, 2017 at 09:45 AM
-- Server version: 5.7.12-0ubuntu1.1
-- PHP Version: 7.0.8-2+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elsaid`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(10) UNSIGNED NOT NULL,
  `album` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `album`, `gambar`, `created_at`, `updated_at`) VALUES
(1, 'Gudang', '1476780464italian-landscape-mountains-nature-large.jpg', '2016-10-18 08:47:45', '2016-10-18 08:47:45'),
(2, 'PRODUK', '1476784063A.JPG', '2016-10-18 09:47:44', '2016-10-18 09:47:44');

-- --------------------------------------------------------

--
-- Table structure for table `categoris`
--

CREATE TABLE `categoris` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `phone`, `subject`, `message`, `status`, `created_at`, `updated_at`) VALUES
(2, 'Syahroni', 'admin@email.com', '08653657865', 'test', 'tes message', '2', '2016-10-20 06:24:31', '2016-10-20 06:24:31');

-- --------------------------------------------------------

--
-- Table structure for table `downloads`
--

CREATE TABLE `downloads` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hits` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `album_id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `album_id`, `judul`, `gambar`, `keterangan`, `created_at`, `updated_at`) VALUES
(2, 1, 'Gudang', '1476782391A.jpeg', 'Lorem ipsum dolor sit amet, conc tetu er adipi scing.', '2016-10-18 09:19:52', '2016-10-18 09:19:52'),
(3, 1, 'Gudang2', '1476782716b.jpeg', 'Lorem ipsum dolor sit amet, conc tetu er adipi scing.', '2016-10-18 09:25:16', '2016-10-18 09:25:16'),
(4, 1, 'Gudang3', '1476782731c.jpeg', 'Lorem ipsum dolor sit amet, conc tetu er adipi scing.', '2016-10-18 09:25:32', '2016-10-18 09:25:32'),
(5, 1, 'Gudang4', '1476782745d.jpeg', 'Lorem ipsum dolor sit amet, conc tetu er adipi scing.', '2016-10-18 09:25:46', '2016-10-18 09:25:46'),
(6, 1, 'Gudang5', '1476782757e.jpeg', 'Lorem ipsum dolor sit amet, conc tetu er adipi scing. Lorem ipsum dolor sit amet, conc tetu er adipi scing.', '2016-10-18 09:25:58', '2016-10-18 09:44:53'),
(7, 2, 'Produk1', '1476784123a10.png', 'A1', '2016-10-18 09:48:44', '2016-10-18 09:48:44'),
(8, 2, 'produk2', '1476784166a20.png', 'a2', '2016-10-18 09:49:27', '2016-10-18 09:50:39'),
(9, 2, 'produk3', '1476784188a30.png', 'a3', '2016-10-18 09:49:48', '2016-10-18 09:50:28'),
(10, 2, 'produk4', '1476784203a40.png', 'a4', '2016-10-18 09:50:04', '2016-10-18 09:50:21'),
(11, 2, 'produk5', '1476784256a50.png', 'a5', '2016-10-18 09:50:56', '2016-10-18 09:50:56'),
(12, 2, 'produk6', '1476784275a60.png', 'a6', '2016-10-18 09:51:15', '2016-10-18 09:51:15'),
(13, 2, 'produk7', '1476784293a70.png', 'a7', '2016-10-18 09:51:34', '2016-10-18 09:51:34'),
(14, 2, 'produk8', '1476784322D.jpg', 'a8', '2016-10-18 09:52:02', '2016-10-18 09:52:02');

-- --------------------------------------------------------

--
-- Table structure for table `identitas`
--

CREATE TABLE `identitas` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addres2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `day` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `identitas`
--

INSERT INTO `identitas` (`id`, `title`, `description`, `keyword`, `author`, `name`, `addres`, `addres2`, `note`, `email`, `phone`, `day`, `time`, `created_at`, `updated_at`) VALUES
(1, 'Official Website Elsaid Kraton Mas', 'Developed into a garment export company with a good quality.', 'garment, elsaid, konveksi Elsaid Kraton Mas', 'Elsaid Kraton Mas', 'PT Elsaid Kraton Mas Indonesia', '<b>Indonesia :</b> JL Gedangan No. 9, RT 002 RW 002 Kwarasan Grogol - Sukoharjo', '<b>Mesir </b>: Boody Export Shara Algamuria Watarh Al Bahar Boody Export', 'Note', 'ptelsaidkratonmasindonesia@gmail.com', '(0271) 6727412 / (0271) 6727385', 'Monday - Saturday', '07:45 AM - 04:00 PM', '2016-11-07 00:00:00', '2016-11-10 02:05:48');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_09_10_160004_create_posts_table', 1),
('2016_09_10_160138_create_categoris_table', 1),
('2016_09_14_154839_create_statis_table', 1),
('2016_09_15_090104_create_slideshows_table', 1),
('2016_09_18_030702_create_albums_table', 1),
('2016_09_18_030741_create_galleries_table', 1),
('2016_09_19_021929_create_downloads_table', 1),
('2016_09_20_065043_laratrust_setup_tables', 1),
('2016_10_20_042921_create_contacts_table', 2),
('2016_11_07_115635_create_identitas_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `categori_id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `penulis` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `hits` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', NULL, '2016-10-13 08:11:47', '2016-10-13 08:11:47'),
(2, 'member', 'Member', NULL, '2016-10-13 08:11:47', '2016-10-13 08:11:47');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(3, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `slideshows`
--

CREATE TABLE `slideshows` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slideshows`
--

INSERT INTO `slideshows` (`id`, `judul`, `keterangan`, `keterangan2`, `link`, `image`, `publish`, `created_at`, `updated_at`) VALUES
(1, 'Gambar 1', 'sASA', '', '#', '1476779695slide1.jpg', 'Y', '2016-10-18 08:34:55', '2016-10-18 08:34:55'),
(2, 'gambar 2', 'abc', 'as', '#', '1476779824slide2.jpg', 'Y', '2016-10-18 08:37:04', '2016-10-18 08:37:04'),
(3, 'gambar 3', 'as', 'as', '#', '1476779849slide3.jpg', 'Y', '2016-10-18 08:37:29', '2016-10-18 08:37:29');

-- --------------------------------------------------------

--
-- Table structure for table `statis`
--

CREATE TABLE `statis` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hits` int(11) NOT NULL,
  `penulis` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `statis`
--

INSERT INTO `statis` (`id`, `judul`, `slug`, `content`, `image`, `hits`, `penulis`, `created_at`, `updated_at`) VALUES
(1, 'Wellcome to Our Site', 'wellcome-to-our-site', '<p>Perspiciatis unde omnis iste natus error sit voluptatem. Cum sociis natoque penatibus et magnis dis parturient montes ascetur ridiculus musull dui. Lorem ipsumulum aenean noummy endrerit mauris. Cum sociis natoque penatibuLorem ipsumulum aenean noummy endrerit mauris. Cum sociis natoque penatibus et magnis dis parturient montes ascetur ridiculus mus. Null dui. Fusce feugiat malesuada odio.</p>\r\n\r\n<p>penatibus et magnis dis parturient montes ascetur ridiculus musull dui. Lorem ipsumulum aenean noummy endrerit mauris. Cum sociis natoque penatibuLorem ipsumulum aenean noummy endrerit mauris. Cum sociis natoque penatibus et magnis dis parturient montes ascetur ridiculus mus. Null dui. Fusce feugiat malesuada odio.</p>\r\n', '', 0, 'Admin Elsaid', '2016-10-18 09:02:16', '2016-10-18 10:09:39'),
(2, 'THE HISTORY OF PT ELSAID KRATON MAS INDONESIA', 'the-history-of-pt-elsaid-kraton-mas-indonesia', '<p>PT Elsaid Kraton Mas Indonesia is an export garment company. We produce T-shirt, underwear (short and bra), nightwear, abaya, men shirt, pants, children and baby and other promotional products. PT Elsaid Kraton Mas Indonesia established in 2014 with&nbsp;<strong>Mr. El Said Ahmed El Said Behiry</strong>&nbsp;as the owner of the company. It produces garment from polyester and vescos&nbsp;till now we grow and begin to produce material form knit and woven. Also, we accept kind of sewing services for men shirt, T-shirt, nightwear and underwear.</p>\r\n\r\n<p>Our company are supported by the expert workers in their own fields. Besides, we are also supported by our trading office located in Egypt to help us in providing our necessities&nbsp;needed in our production process, like the machines as an example.</p>\r\n\r\n<p>PT Elsaid Kraton Mas Indonesia consists of several departments as belows :</p>\r\n\r\n<p>1. Main Office :</p>\r\n\r\n<p>- Human Resources Department</p>\r\n\r\n<p>- Accounting Department</p>\r\n\r\n<p>- Export-Import Department</p>\r\n\r\n<p>-&nbsp;Head Of Production</p>\r\n\r\n<p>2. Warehouse&nbsp;:</p>\r\n\r\n<p>- Sub Material Department (Belongs to accessories and fabrics)</p>\r\n\r\n<p>3. Cutting Department:</p>\r\n\r\n<p>Cutting department works based on Standart Operating Procedures (SOP) and Purchasing Order (PO) received form Head of Production.</p>\r\n\r\n<p>It has 3 cutting tables, with 2 M width and 12 M lenght. Also, we have the experts of cutting manpower.&nbsp;</p>\r\n\r\n<p>4. Sewing Department :</p>\r\n\r\n<p>We bring the expert, professional and experienced production manpower with appropriated quality control for each project. We ensure that all of our projects are completed with satisfactory result.&nbsp;</p>\r\n\r\n<p>There are 140 machines we have in sewing department, divided into 6 lines. 3 Line for export production and 3 lines for CMT production (sewing services)<br />\r\n&nbsp;</p>\r\n\r\n<p>5. Finishing Department</p>\r\n\r\n<p>It handles all matters&nbsp;related to packing process (from folding, put on polybag, blister bag to cartoon) till shipment process.</p>\r\n', '1477011105italian-landscape-mountains-nature-large.jpg', 0, 'Admin Elsaid', '2016-10-18 10:14:28', '2016-10-21 00:51:45'),
(3, 'VISION AND MISSION', 'vision-and-mission', '<p><strong>VISION</strong></p>\r\n\r\n<p>Developed into a garment export company with a good quality.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>MISSION&nbsp;</strong></p>\r\n\r\n<p>1. Provide&nbsp;the best thing for our consumer.</p>\r\n\r\n<p>2. Create the jobs for citizens.</p>\r\n\r\n<p>3. Provide the best quality for consumer and satisfy consumer desires</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>MOTTO</strong></p>\r\n\r\n<p><strong><em>TODAY MUST BE BETTER THAN YESTERDAY, AND TOMORROW MUST BE BETTER THAN TODAY.</em></strong></p>\r\n', '', 0, 'Admin Elsaid', '2016-10-19 14:06:52', '2016-10-19 14:06:52'),
(4, 'COMPANY VALUES', 'company-values', '<p>As a growing up company,&nbsp;<em><strong>PT Elsaid Kraton Mas Indonesia</strong></em>&nbsp;has 5 main values, as described belows :</p>\r\n\r\n<p><strong>1. INTEGRITY</strong></p>\r\n\r\n<p>- We believe by hold on a&nbsp;strong commitment, we can produce the product as expected by buyer.&nbsp;</p>\r\n\r\n<p><strong>2. TEAMWORK</strong></p>\r\n\r\n<p>- A great teamwork both of internal party and external party (suppliers and buyers) will lead to success for the company.&nbsp;&nbsp;</p>\r\n\r\n<p><strong>3. CONSISTENCY</strong></p>\r\n\r\n<p>- We always try to do our best to keep our consistency in every single thing we do and we produce.&nbsp;</p>\r\n\r\n<p><strong>4. QUALITY</strong></p>\r\n\r\n<p>- We always try to maintain our best quality with a sustainable&nbsp;commitment and creativity.&nbsp;</p>\r\n\r\n<p><strong>5. HUMAN RESOURCES</strong></p>\r\n\r\n<p>- By having the competent and credible husman resources, we can generate the products&nbsp;appropriate&nbsp;to market and consumer demand.</p>\r\n', '', 0, 'Admin Elsaid', '2016-10-19 14:07:21', '2016-10-19 14:07:21'),
(5, 'PRODUCT', 'product', '<p>PT Elsaid Kraton Mas Indonesia has 3 (three) kinds of product as follows :</p>\r\n\r\n<p>1. For Children :</p>\r\n\r\n<p>- Boy Shirt</p>\r\n\r\n<p>- Poloshirt &amp; T-shirt</p>\r\n\r\n<p>- knitted Garment</p>\r\n\r\n<p>2. For Men :</p>\r\n\r\n<p>- Shirt</p>\r\n\r\n<p>- Pants</p>\r\n\r\n<p>- Knitted Garment</p>\r\n\r\n<p>- Poloshirt &amp; T-shirt</p>\r\n\r\n<p>3. For Ladies :</p>\r\n\r\n<p>- Abaya</p>\r\n\r\n<p>- Poloshirt &amp; T-shirt</p>\r\n\r\n<p>- underwear</p>\r\n\r\n<p>- nightwear</p>\r\n\r\n<p>- blazer</p>\r\n', '', 0, 'Admin Elsaid', '2016-10-19 14:07:54', '2016-10-19 14:07:54'),
(6, 'Urgently required !!', 'urgently-required', '<h3><strong>Administration Officer</strong></h3>\r\n\r\n<p><strong>requirement&nbsp;:</strong></p>\r\n\r\n<ol>\r\n	<li>Diploma/Bachelor Degree of Accounting Major</li>\r\n	<li>Precise, diligent, self motivated, competent, experienced in accounting</li>\r\n	<li>Female</li>\r\n</ol>\r\n\r\n<p>Please send your application to our website or submit by email address elsaidkratonmasindonesia@gmail.com</p>\r\n\r\n<p>No more than 2 weeks after this vacancy uploaded.</p>\r\n', '', 0, 'Admin Elsaid', '2016-10-19 14:33:43', '2016-10-19 14:33:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin Elsaid', 'marsonosaputro@gmail.com', '$2y$10$SHXm.aM0f3/sdLIeUGRIWeSJyIx7BdWa17nwYLUx4zpWNJbRfs3d2', 'hdDRx1VqGZKIlBmcCSDZhhiTTKBfsEvu13MdDnAgiT3jhisa27G81JRLnGKL', '2016-10-13 08:11:47', '2016-11-10 02:54:19'),
(2, 'Sample Member', 'member@gmail.com', '$2y$10$r2e7iPMx7jFcA/iaz5igxO2PKgS6i2POLIlM5l2cpv1wbiF9sRueS', NULL, '2016-10-13 08:11:47', '2016-10-13 08:11:47'),
(3, 'Administrator', 'administrator@gmail.com', '$2y$10$st700gRDVIgNgEcvICipPOTPIpcFN0YqzOOuPMa1ytRDDz5nVQ0lK', NULL, '2016-10-20 09:34:00', '2016-10-20 09:34:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categoris`
--
ALTER TABLE `categoris`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categoris_title_unique` (`title`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `downloads`
--
ALTER TABLE `downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galleries_album_id_foreign` (`album_id`);

--
-- Indexes for table `identitas`
--
ALTER TABLE `identitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_categori_id_foreign` (`categori_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `slideshows`
--
ALTER TABLE `slideshows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statis`
--
ALTER TABLE `statis`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `statis_slug_unique` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categoris`
--
ALTER TABLE `categoris`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `downloads`
--
ALTER TABLE `downloads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `identitas`
--
ALTER TABLE `identitas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `slideshows`
--
ALTER TABLE `slideshows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `statis`
--
ALTER TABLE `statis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `galleries`
--
ALTER TABLE `galleries`
  ADD CONSTRAINT `galleries_album_id_foreign` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_categori_id_foreign` FOREIGN KEY (`categori_id`) REFERENCES `categoris` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
