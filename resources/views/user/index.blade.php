@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Data User </h3>
    </div>
    <div class="panel-body">
      @if (session()->has('flash_notification.message'))
          <div class="alert alert-{{ session()->get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! session()->get('flash_notification.message') !!}
          </div>
      @endif
    <a href="{{ route('user.create') }}" class="btn btn-primary" style="margin-bottom:10px">Tambah</a>
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Lengkap</th>
              <th>Email</th>
              <th>Sejak</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($user as $d)
              {!! Form::open(array('url'=>'admin/user/'.$d->id, 'method'=>'delete')) !!}
              {!! Form::hidden('_delete', 'DELETE') !!}
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $d->name }}</td>
                <td>{{ $d->email }}</td>
                <td>{{ $d->created_at }}</td>
                <td>
                  <a href="{{ url('admin/user/'.$d->id.'/edit') }}" class="btn btn-success btn-sm glyphicon glyphicon-pencil"></a>
                  <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-sm btn-danger glyphicon glyphicon-remove"></button>
                </td>
              </tr>
              {!! Form::close() !!}
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
