@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Tambah user baru</h3>
    </div>
    <div class="panel-body">
      {!! Form::open(['method' => 'POST', 'route' => 'user.store', 'class' => 'form-horizontal']) !!}

          @include('user.form')

          <div class="btn-group pull-right">
              <a href="{{ route('user.index') }}" class="btn btn-warning">Batal</a>
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>
      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>

@endsection
