@extends('frontend')
@section('content')
  <!-- Custom styles for our template -->
  <link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/assets/css/isotope.css') }}" media="screen" />
  <link rel="stylesheet" href="{{ URL::asset('frontend/assets/js/fancybox/jquery.fancybox.css') }}" type="text/css" media="screen" />
  <!-- Custom styles for our template -->

  <header id="head" class="secondary">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<h1>Gallery</h1>
				</div>
			</div>
		</div>
	</header>

	<!-- container -->
	<section class="container">
		<div class="row">
			<div class="col-md-12">
				<section id="portfolio" class="page-section section appear clearfix">
          <br>
					<div class="row">
						<nav id="filter" class="col-md-12 text-center">
							<ul>
								<li><a href="{{ url('galeri') }}" class="{{ (Request::is('galeri')) ? 'current' : '' }} btn-theme btn-small">All</a></li>
                @foreach ($album as $d)
                  <li><a href="{{ url('galeri/'.$d->id) }}" class="{{ (Request::is('galeri/'.$d->id)) ? 'current' : '' }} btn-theme btn-small">{{ $d->album }}</a></li>
                @endforeach
							</ul>
						</nav>
						<div class="col-md-12">
							<div class="row">
								<div class="portfolio-items isotopeWrapper clearfix" id="3">
                  @foreach ($gallery as $d)
                    <article class="col-sm-4 isotopeItem webdesign">
  										<div class="portfolio-item">
  											<img src="{{ URL::asset('images/gallery/thumb/'.$d->gambar) }}" alt="" />
  											<div class="portfolio-desc align-center">
  												<div class="folio-info">
  													<a href="{{ URL::asset('images/gallery/'.$d->gambar) }}" class="fancybox">
  														<h5>{{ $d->judul }}</h5>
  														<i class="fa fa-link fa-2x"></i></a>
  												</div>
  											</div>
  										</div>
  									</article>
                  @endforeach
								</div>
							</div>
              <hr>
              <div class="row">
                <div class="col-md-12">
                  <div class="pull-left">
                    <h3>Share this to:</h3>
                    <!-- AddToAny BEGIN -->
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                      <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                      <a class="a2a_button_facebook"></a>
                      <a class="a2a_button_facebook_messenger"></a>
                      <a class="a2a_button_whatsapp"></a>
                      <a class="a2a_button_line"></a>
                      <a class="a2a_button_twitter"></a>
                      <a class="a2a_button_wechat"></a>
                      <a class="a2a_button_telegram"></a>
                      <a class="a2a_button_google_plus"></a>
                      <a class="a2a_button_sms"></a>
                    </div>
                    <script async src="https://static.addtoany.com/menu/page.js"></script>
                    <!-- AddToAny END -->
                  </div>
                  <div class="pull-right">
                    {!! $gallery->render() !!}
                  </div>
                </div>
              </div>
						</div>
					</div>
				</section>
			</div>
		</div>

	</section>
	<!-- /container -->

@endsection
