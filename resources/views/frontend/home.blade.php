@extends('frontend')
@section('content')
<!-- Header -->
<header id="head">
  <div class="container">
        <div class="fluid_container">
                  <div class="camera_wrap camera_emboss pattern_1" id="camera_wrap_4">
                    @foreach ($slideshow as $d)
                      <div data-thumb="{{ URL::asset('images/slideshow/thumb/'.$d->image) }}" data-src="{{ URL::asset('images/slideshow/'.$d->image) }}"></div>
                    @endforeach
                  </div><!-- #camera_wrap_3 -->
              </div><!-- .fluid_container -->
  </div>
</header>
<!-- /Header -->
<section id="search">
<div class="search-panel">

          </div>
</section>
    <section class="secpadding">
  <div class="container">
    <div class="row">
      <div class="col-md-12"><div class="title-box clearfix "><h2 class="title-box_primary">{{ $welcome->judul }}</h2></div>
        {!! $welcome->content !!}
      </div>
      </div>
  </div>
  </section>
    <section class="news-box secpadding">
      <div class="container">
          <h2><span>Our Office</span></h2>
          <div class="row">
            @foreach ($gallery as $d)
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                  <div class="newsBox">
                      <div class="thumbnail">
                          <figure><img src="{{ URL::asset('images/gallery/thumb/'.$d->gambar) }}" alt=""></figure>
                          <div class="caption maxheight2">
                              <div class="box_inner">
                                      <div class="box">
                                          <p class="title"><strong>{{ $d->judul }}</strong></p>
                                          {!! $d->keterangan !!}
                                      </div>
                                      <div>
                                          <a href="{{ url('gallery/'.$d->album_id) }}" class="btn-inline">more</a>
                                      </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            @endforeach
          </div>
      </div>
  </section>

<section id="packages" class="secpadding">
      <div class="container">
           <h2><span>Our Product</span></h2>
          <div class="row">
            @foreach ($produk as $d)
              <div class="col-md-3 col-sm-6">
                          <div class="cuadro_intro_hover " style="background-color:#cccccc;">
                              <p style="text-align:center;">
                                  <img src="{{ URL::asset('images/gallery/thumb/'.$d->gambar) }}" class="img-responsive" alt="">
                              </p>
                              <div class="caption">
                                  <div class="blur"></div>
                                  <div class="caption-text">
                                      <h3>{{ $d->judul }}</h3>
                                      <a class=" btn btn-default" href="{{ url('gallery/'.$d->album_id) }}">More</i></a>
                                  </div>
                              </div>
                          </div>

              </div>
            @endforeach


          </div>
         <div>
  </section>
@endsection
