@extends('frontend')
@section('content')
  <header id="head" class="secondary">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<h1>Contact us</h1>
				</div>
			</div>
		</div>
	</header>

	<!-- container -->
	<div class="container">
				<div class="row">
					<div class="col-md-6">
            @if (session()->has('flash_notification.message'))
              <br><br>
                <div class="alert alert-{{ session()->get('flash_notification.level') }}">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {!! session()->get('flash_notification.message') !!}
                </div>
            @endif
						<h3 class="section-title">Get in touch with us by filling contact form below</h3>

              {!! Form::open(['method' => 'POST', 'route' => 'kontak.store', 'class' => 'form-light mt-20', 'role'=>'form']) !!}

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  {!! Form::label('name', 'Name') !!}
                  {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                  <small class="text-danger">{{ $errors->first('name') }}</small>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                      {!! Form::label('email', 'Email Addres') !!}
                      {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email Addres']) !!}
                      <small class="text-danger">{{ $errors->first('email') }}</small>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                      {!! Form::label('phone', 'Phone') !!}
                      {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone Number']) !!}
                      <small class="text-danger">{{ $errors->first('phone') }}</small>
                  </div>
                </div>
              </div>

                <div class="form-group{{ $errors->has('subject') ? ' has-error' : '' }}">
                    {!! Form::label('subject', 'Subject') !!}
                    {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Subject']) !!}
                    <small class="text-danger">{{ $errors->first('subject') }}</small>
                </div>
              <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                  {!! Form::label('message', 'Message') !!}
                  {!! Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Write you message here...', 'style'=>'height:100px;']) !!}
                  <small class="text-danger">{{ $errors->first('message') }}</small>
              </div>

              <button type="submit" class="btn btn-two">Send message</button><p><br/></p>
              {!! Form::close() !!}

					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-6">
								<h3 class="section-title">Office Address</h3>
								<div class="contact-info">
									<h5>{{ $identitas->name }}</h5>
									<p>{!! $identitas->addres !!}</p>
                  <p>{!! $identitas->addres2 !!}</p>
									<h5>Email</h5>
									<p>{{ $identitas->email }}</p>

									<h5>Phone</h5>
									<p>{{ $identitas->phone }}</p>
								</div>
							</div>
							<div class="col-md-6">
								<h3 class="section-title">Timings</h3>
								<div class="contact-info">
									<h5>{{ $identitas->day }}</h5>
									<p>{{ $identitas->time }}</p>
								</div>
                <div class="contact-info">
									<h5>Note</h5>
									<p>{{ $identitas->note }}</p>
								</div>
							</div>
						</div>
						<h3 class="section-title">Share this:</h3>
						<p>
              <!-- AddToAny BEGIN -->
              <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                <a class="a2a_button_facebook"></a>
                <a class="a2a_button_facebook_messenger"></a>
                <a class="a2a_button_whatsapp"></a>
                <a class="a2a_button_line"></a>
                <a class="a2a_button_twitter"></a>
                <a class="a2a_button_wechat"></a>
                <a class="a2a_button_telegram"></a>
                <a class="a2a_button_google_plus"></a>
                <a class="a2a_button_sms"></a>
              </div>
              <script async src="https://static.addtoany.com/menu/page.js"></script>
              <!-- AddToAny END -->
						</p>
					</div>
				</div>
			</div>
	<!-- /container -->
@endsection
