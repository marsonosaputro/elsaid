@extends('frontend')
@section('content')
  <header id="head" class="secondary">
      <div class="container">
          <div class="row">
              <div class="col-sm-8">
                  <h1>Carrier</h1>
              </div>
          </div>
      </div>
  </header>

  <!-- container -->
  <section class="container">
      <div class="row">
          <!-- main content -->
          <section class="col-sm-8 maincontent">
              <h3>{{ $carrier->judul }}</h3>
              @if (!empty($carrier->image))
                <img src="{{ URL::asset('images/statis/'.$carrier->image) }}" class="img img-responsive img-thumbnail" style="width:230px; float: right; margin: 0 20px;" />
              @endif
               {!! $carrier->content !!}
          </section>

          <!-- /main -->
      </div>
      <hr>
      <h3>Share this to:</h3>
      <!-- AddToAny BEGIN -->
      <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
        <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
        <a class="a2a_button_facebook"></a>
        <a class="a2a_button_facebook_messenger"></a>
        <a class="a2a_button_whatsapp"></a>
        <a class="a2a_button_line"></a>
        <a class="a2a_button_twitter"></a>
        <a class="a2a_button_wechat"></a>
        <a class="a2a_button_telegram"></a>
        <a class="a2a_button_google_plus"></a>
        <a class="a2a_button_sms"></a>
      </div>
      <script async src="https://static.addtoany.com/menu/page.js"></script>
      <!-- AddToAny END -->
  </section>
@endsection
