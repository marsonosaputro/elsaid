@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Form Identitas</h3>
    </div>
    <div class="panel-body">
      @if (session()->has('flash_notification.message'))
          <div class="alert alert-{{ session()->get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! session()->get('flash_notification.message') !!}
          </div>
      @endif

      {!! Form::model($identitas, ['route' => ['identitas.update', $identitas->id], 'class'=>'form-horizontal', 'method' => 'PUT']) !!}

          <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
              {!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('title', null, ['class' => 'form-control' ]) !!}
                  <small class="text-danger">{{ $errors->first('title') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
              {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('description', null, ['class' => 'form-control' ]) !!}
                  <small class="text-danger">{{ $errors->first('description') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('keyword') ? ' has-error' : '' }}">
              {!! Form::label('keyword', 'Keyword', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('keyword', null, ['class' => 'form-control' ]) !!}
                  <p class="small">pisahkan dengan tanda koma</p>
                  <small class="text-danger">{{ $errors->first('keyword') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('author') ? ' has-error' : '' }}">
              {!! Form::label('author', 'Author', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('author', null, ['class' => 'form-control' ]) !!}
                  <small class="text-danger">{{ $errors->first('author') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              {!! Form::label('name', 'Company Name', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('name', null, ['class' => 'form-control' ]) !!}
                  <small class="text-danger">{{ $errors->first('name') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('addres') ? ' has-error' : '' }}">
              {!! Form::label('addres', 'Addres', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('addres', null, ['class' => 'form-control' ]) !!}
                  <small class="text-danger">{{ $errors->first('addres') }}</small>
              </div>
          </div>
          <div class="form-group{{ $errors->has('addres2') ? ' has-error' : '' }}">
              {!! Form::label('addres2', 'Addres 2', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('addres2', null, ['class' => 'form-control' ]) !!}
                  <small class="text-danger">{{ $errors->first('addres2') }}</small>
              </div>
          </div>
          <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
              {!! Form::label('note', 'Note', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('note', null, ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('note') }}</small>
              </div>
          </div>
           <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
               {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
               <div class="col-sm-9">
                   {!! Form::text('email', null, ['class' => 'form-control' ]) !!}
                   <small class="text-danger">{{ $errors->first('email') }}</small>
               </div>
           </div>

           <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
               {!! Form::label('phone', 'Phone', ['class' => 'col-sm-3 control-label']) !!}
               <div class="col-sm-9">
                   {!! Form::text('phone', null, ['class' => 'form-control' ]) !!}
                   <small class="text-danger">{{ $errors->first('phone') }}</small>
               </div>
           </div>

           <div class="form-group{{ $errors->has('day') ? ' has-error' : '' }}">
               {!! Form::label('day', 'Days', ['class' => 'col-sm-3 control-label']) !!}
               <div class="col-sm-9">
                   {!! Form::text('day', null, ['class' => 'form-control' ]) !!}
                   <small class="text-danger">{{ $errors->first('day') }}</small>
               </div>
           </div>

           <div class="form-group{{ $errors->has('time') ? ' has-error' : '' }}">
               {!! Form::label('time', 'Time', ['class' => 'col-sm-3 control-label']) !!}
               <div class="col-sm-9">
                   {!! Form::text('time', null, ['class' => 'form-control' ]) !!}
                   <small class="text-danger">{{ $errors->first('time') }}</small>
               </div>
           </div>

          <div class="btn-group pull-right">
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>



      {!! Form::close() !!}

    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
