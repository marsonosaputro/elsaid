<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <!--
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
<span class="input-group-btn">
  <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
</span>
            </div>
        </form>
      -->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links
            <li><a href="{{ url('admin/kategori') }}"><i class="fa fa-th"></i> <span>Kategori</span></a></li>
            <li><a href="{{ url('admin/post') }}"><i class="fa fa-laptop"></i><span>Berita</span></a></li>
            -->
            <li><a href="{{ url('admin/identitas') }}"><i class="fa fa-gear"></i><span>Identitas Website</span></a></li>
            <li><a href="{{ url('admin/statis') }}"><i class="fa fa-edit"></i><span>Halaman Statis</span></a></li>
            <li><a href="{{ url('admin/slideshow') }}"><i class="fa fa-desktop"></i><span>Slideshow</span></a></li>
            <li><a href="{{ url('admin/album') }}"><i class="fa fa-camera"></i><span>Album</span></a></li>
            <li><a href="{{ url('admin/gallery') }}"><i class="fa fa-camera-retro"></i><span>Gallery</span></a></li>
            <!-- <li><a href="{{ url('admin/download') }}"><i class="fa fa-download"></i><span>Download</span></a></li> -->
            <li><a href="{{ url('kontak') }}"><i class="fa fa-envelope"></i><span>Kontak</span></a></li>
            <li><a href="{{ url('admin/user') }}"><i class="fa fa-users"></i><span>User</span></a></li>

            <li><a href="{{ url('admin/logout') }}"><i class="fa fa-sign-out"></i><span>Logout</span></a></li>
            <!--
            <li class="treeview">
                <a href="#"><span>Multilevel</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">Link in level 2</a></li>
                    <li><a href="#">Link in level 2</a></li>
                </ul>
            </li>
          -->
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
