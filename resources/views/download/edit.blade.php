@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Edit File Download</h3>
    </div>
    <div class="panel-body">
      {!! Form::model($download, ['route' => ['download.update', $download->id], 'method' => 'PUT', 'files'=>true, 'class'=>'form-horizontal']) !!}

      <div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
          {!! Form::label('album', 'Keterangan File', ['class' => 'col-sm-3 control-label']) !!}
          <div class="col-sm-9">
              {!! Form::text('judul', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('judul') }}</small>
          </div>
      </div>

      <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
          {!! Form::label('file', 'Upload File', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::file('file', ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('file') }}</small>
              </div>
      </div>

      <div class="btn-group pull-right">
          <a href="{{ URL::route('download.index') }}" class="btn btn-warning">Batal</a>
          {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
      </div>  {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
