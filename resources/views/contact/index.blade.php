@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Management Contact</h3>
    </div>
    <div class="panel-body">
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Subject</th>
              <th>Status</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($contact as $d)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $d->name }}</td>
                <td>{{ $d->subject }}</td>
                <td>
                  @if ($d->status == '1')
                    <button type="button" class="btn btn-sm btn-success fa fa-dot-circle-o"></button>
                  @else
                    <button type="button" class="btn btn-sm btn-primary fa fa-check"></button>
                  @endif
                </td>
                <td>

                  {!! Form::open(array('url'=>'kontak/'.$d->id, 'method'=>'delete')) !!}
                  {!! Form::hidden('_delete', 'DELETE') !!}
                  <a href="{{ route('kontak.show', $d->id) }}" class="btn btn-info fa fa-folder-open"></a>
                  <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-sm btn-danger glyphicon glyphicon-remove"></button>
                  {!! Form::close() !!}
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
