@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Open Contact</h3>
    </div>
    <div class="panel-body">
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
            <tr>
              <th>Nama</th><td>{{ $contact->name }}</td>
            </tr>
            <tr>
                <th>Email</th><td>{{ $contact->email }}</td>
            </tr>
            <tr>
              <th>Phone</th><td>{{ $contact->phone }}</td>
            </tr>
            <tr>
              <th>Subject</th><td>{{ $contact->subject }}</td>
            </tr>
            <tr>
              <th>Message</th><td>{{ $contact->message }}</td>
            </tr>
        </table>
      </div>
      <div class="pull-right">
        <a href="{{ url('kontak') }}" class="btn btn-warning">Back</a>
      </div>
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
