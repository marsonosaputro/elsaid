@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Tambah Gallery Foto Kegiatan</h3>
    </div>
    <div class="panel-body">
      {!! Form::open(['method' => 'POST', 'route' => 'gallery.store', 'class' => 'form-horizontal', 'files'=>true]) !!}

          <div class="form-group{{ $errors->has('album_id') ? ' has-error' : '' }}">
              {!! Form::label('album_id', 'Pilih Album', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::select('album_id', $album, null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('album_id') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
              {!! Form::label('judul', 'Judul Gallery', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('judul', null, ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('judul') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
              {!! Form::label('gallery', 'Keterangan', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('keterangan', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('keterangan') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('gambar') ? ' has-error' : '' }}">
              {!! Form::label('gambar', 'Upload Foto', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                      {!! Form::file('gambar', ['class' => 'form-control']) !!}
                      <p class="help-block">Ukuran gambar: </p>
                      <small class="text-danger">{{ $errors->first('gambar') }}</small>
                  </div>
          </div>

          <div class="btn-group pull-right">
              <a href="{{ URL::route('gallery.index') }}" class="btn btn-warning">Batal</a>
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>
      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
