<div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
    {!! Form::label('judul', 'Judul Halaman', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('judul', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('judul') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
    {!! Form::label('content', 'Deskripsi', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::textarea('content', null, ['class' => 'form-control', 'id'=>'editor1']) !!}
        <small class="text-danger">{{ $errors->first('content') }}</small>
    </div>
</div>

@if (!empty($statis->image))
  <div class="form-group">
      {!! Form::label('inputname', 'Gambar Sebelumnya', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
          <img src="{{ asset('images/statis/'.$statis->image) }}" class="img img-responsive img-thumbnail" style="width:200px" />
      </div>
  </div>
@endif

<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    {!! Form::label('image', 'Gambar', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-9">
            {!! Form::file('image', ['class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('image') }}</small>
        </div>
</div>
