@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Halaman Dashboard</h3>
    </div>
    <div class="panel-body">
      <p>
        Selamat datang di halaman Adminweb <b> {{ Auth::user()->name }}</b>,
      </p>
    </div>
    <div class="panel-footer">

    </div>
  </div>

@endsection
