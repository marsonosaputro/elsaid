@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Tambah Kategori</h3>
    </div>
    <div class="panel-body">
      {!! Form::open(['method' => 'POST', 'route' => 'kategori.store', 'class' => 'form-horizontal']) !!}

          <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
              {!! Form::label('title', 'Judul Kategori', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('title', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('title') }}</small>
              </div>
          </div>

          <div class="btn-group pull-right">
              <a href="{{ url('admin/kategori') }}" class="btn btn-warning">Batal</a>
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>
      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>

@endsection
