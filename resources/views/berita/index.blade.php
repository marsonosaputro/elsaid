@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Daftar Berita</h3>
    </div>
    <div class="panel-body">
      @if (session()->has('flash_notification.message'))
          <div class="alert alert-{{ session()->get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! session()->get('flash_notification.message') !!}
          </div>
      @endif
      <a href="{{ url('admin/post/create') }}" class="btn btn-primary" style="margin-bottom: 10px;">Tambah</a>
      <div class='table-responsive'>
        <table class='table table-striped table-bordered table-hover table-condensed'>
          <thead>
            <tr>
              <th>No</th>
              <th>Judul</th>
              <th>Kategori</th>
              <th>Hits</th>
              <th>Penulis</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($berita as $d)
              {!! Form::open(array('url'=>'admin/post/'.$d->id, 'method'=>'delete')) !!}
              {!! Form::hidden('_delete', 'DELETE') !!}
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $d->judul  }}</td>
                <td>{{ $d->categori->title }}</td>
                <td>{{ $d->hits }}</td>
                <td>{{ $d->penulis}}</td>
                <td>
                  <a href="{{ url('admin/post/'.$d->id.'/edit') }}" class="btn btn-success btn-sm glyphicon glyphicon-pencil"></a>
                  <button type="submit" onclick="javascript: return confirm('Yakin akan di hapus?')" class="btn btn-sm btn-danger glyphicon glyphicon-remove"></button>
                </td>
              </tr>
              {!! Form::close() !!}
            @endforeach

          </tbody>
        </table>
      </div>
      <div class="pull-right">
        {!! $berita->render() !!}
      </div>
    </div>
    <div class="panel-footer">

    </div>
  </div>

@endsection
