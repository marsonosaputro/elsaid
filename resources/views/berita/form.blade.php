<div class="form-group{{ $errors->has('categori_id') ? ' has-error' : '' }}">
    {!! Form::label('categori_id', 'Kategori', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::select('categori_id', $kategori, null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('categori_id') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
    {!! Form::label('judul', 'Judul Berita', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('judul', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('judul') }}</small>
    </div>
</div>
<div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
    {!! Form::label('content', 'Deskripsi Berita', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::textarea('content', null, ['class' => 'form-control', 'id'=>'editor1']) !!}
        <small class="text-danger">{{ $errors->first('content') }}</small>
    </div>
</div>
@if (!empty($berita->image))
  <div class="form-group">
      {!! Form::label('image', 'Gambar Sebelumnya', ['class' => 'col-sm-3 control-label']) !!}
          <div class="col-sm-9">
              <img src="{{ asset('images/post/'.$berita->image) }}" class="img img-responsive img-thumbnail" style="width:200px;" />
              <small class="text-danger">{{ $errors->first('image') }}</small>
          </div>
  </div>
@endif
<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    {!! Form::label('image', 'Gambar', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-9">
            {!! Form::file('image',['class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('image') }}</small>
        </div>
</div>
