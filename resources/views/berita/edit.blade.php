@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Edit Berita</h3>
    </div>
    <div class="panel-body">
      {!! Form::model($berita, ['route' => ['post.update', $berita->id], 'method' => 'PUT', 'class'=>'form-horizontal']) !!}

          @include('berita.form')

          <div class="btn-group pull-right">
              <a href="{{ url('admin/post') }}" class="btn btn-warning">Batal</a>
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>

      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
