<!--
Author: WebThemez
Author URL: http://webthemez.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="{{ $identitas->description }}">
	<meta name="author" content="{{ $identitas->author }}">
	<meta name="keyword" content="{{ $identitas->keyword }} {{ isset($page_title) ? $page_title : 'kraton mas' }}">
	<title>{{ isset($page_title) ? $page_title : $identitas->title }}</title>
	<link rel="favicon" href="{{ URL::asset('frontend/assets/images/favicon.png') }}">
	<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
	<link rel="stylesheet" href="{{ URL::asset('frontend/assets/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('frontend/assets/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('frontend/assets/css/bootstrap-theme.css') }}" media="screen">
	<link rel="stylesheet" href="{{ URL::asset('frontend/assets/css/style.css') }}">
  <link rel='stylesheet' id='camera-css'  href='{{ URL::asset('frontend/assets/css/camera.css') }}' type='text/css' media='all'>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<!-- Fixed navbar -->
	<div class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<!-- Button for smallest screens -->
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
				<a class="navbar-brand" href="{{ url('/') }}">
					<img src="{{ URL::asset('frontend/assets/images/logo.png') }}" alt="Techro HTML5 template"></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav pull-right mainNav">
					<li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{ url('/') }}">Home</a></li>
					<li class="{{ Request::is('about') ? 'active' : '' }}"><a href="{{ url('about') }}">About</a></li>
					<li class="{{ Request::is('galeri') ? 'active' : '' }}"><a href="{{ url('galeri') }}">Gallery</a></li>
					<li class="{{ Request::is('carrier') ? 'active' : '' }}"><a href="{{ url('carrier') }}">Carrier</a></li>
					<li class="{{ Request::is('contact') ? 'active' : '' }}"><a href="{{ url('contact') }}">Contact</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
	<!-- /.navbar -->

	@yield('content')


    <footer id="footer">

		<div class="footer2">
			<div class="container">
				<div class="row">

					<div class="col-md-6 panel">
						<div class="panel-body">
							<p class="simplenav">
								<a href="{{ url('/') }}">Home</a> |
								<a href="{{ url('about') }}">About</a> |
								<a href="{{ url('galeri') }}">Gallery</a> |
								<a href="{{ url('carrier') }}">Carrier</a> |
								<a href="{{ url('contact') }}">Contact</a>
							</p>
						</div>
					</div>

					<div class="col-md-6 panel">
						<div class="panel-body">
							<p class="text-right">
								Copyright &copy; 2016. Template by <a href="http://webthemez.com/" rel="develop">WebThemez.com</a>
								<b>Powered By <a href="#">Studio17</a></b>
							</p>
						</div>
					</div>

				</div>
				<!-- /row of panels -->
			</div>
		</div>
	</footer>

	<!-- JavaScript libs are placed at the end of the document so the pages load faster -->
	<script src="{{ URL::asset('frontend/assets/js/modernizr-latest.js') }}"></script>
	<script type='text/javascript' src='{{ URL::asset('frontend/assets/js/jquery.min.js') }}'></script>
  <script type='text/javascript' src="{{ URL::asset('frontend/assets/js/fancybox/jquery.fancybox.pack.js') }}"></script>

  <script type="text/javascript" src="{{ URL::asset('frontend/assets/js/jquery.mobile.customized.min.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('frontend/assets/js/jquery.easing.1.3.js') }}"></script>
  <script type="text/javascript" src="{{ URL::asset('frontend/assets/js/camera.min.js') }}"></script>
  <script src="{{ URL::asset('frontend/assets/js/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('frontend/assets/js/custom.js') }}"></script>

	<script>
		jQuery(function(){

			jQuery('#camera_wrap_4').camera({
				height: '750',
				loader: 'bar',
				pagination: false,
				thumbnails: false,
				hover: false,
				opacityOnGrid: false,
				imagePath: '{{ URL::asset('frontend/assets/images/') }}'
			});

		});
	</script>

</body>
</html>
