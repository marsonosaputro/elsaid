@extends('backend')
@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Tambah Album Foto Kegiatan</h3>
    </div>
    <div class="panel-body">
      {!! Form::open(['method' => 'POST', 'route' => 'album.store', 'class' => 'form-horizontal', 'files'=>true]) !!}

          <div class="form-group{{ $errors->has('album') ? ' has-error' : '' }}">
              {!! Form::label('album', 'Judul Album', ['class' => 'col-sm-3 control-label']) !!}
              <div class="col-sm-9">
                  {!! Form::text('album', null, ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('album') }}</small>
              </div>
          </div>

          <div class="form-group{{ $errors->has('gambar') ? ' has-error' : '' }}">
              {!! Form::label('gambar', 'Cover Album', ['class' => 'col-sm-3 control-label']) !!}
                  <div class="col-sm-9">
                      {!! Form::file('gambar', ['class' => 'form-control']) !!}
                      <p class="help-block">Ukuran gambar: </p>
                      <small class="text-danger">{{ $errors->first('gambar') }}</small>
                  </div>
          </div>

          <div class="btn-group pull-right">
              <a href="{{ URL::route('album.index') }}" class="btn btn-warning">Batal</a>
              {!! Form::submit("Simpan", ['class' => 'btn btn-success']) !!}
          </div>
      {!! Form::close() !!}
    </div>
    <div class="panel-footer">

    </div>
  </div>
@endsection
