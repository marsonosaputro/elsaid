<div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
    {!! Form::label('judul', 'Judul slideshow', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('judul', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('judul') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
    {!! Form::label('keterangan', 'Keterangan', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('keterangan', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('keterangan') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('keterangan2') ? ' has-error' : '' }}">
    {!! Form::label('keterangan2', 'Keterangan 2', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('keterangan2', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('keterangan2') }}</small>
    </div>
</div>

<div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
    {!! Form::label('link', 'Link Artikel', ['class' => 'col-sm-3 control-label']) !!}
    <div class="col-sm-9">
        {!! Form::text('link', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('link') }}</small>
    </div>
</div>

@if (!empty($slide->image))
  <div class="form-group">
      {!! Form::label('image', 'Gambar Sebelumnya', ['class' => 'col-sm-3 control-label']) !!}
          <div class="col-sm-9">
              <img src="{{ asset('images/slideshow/thumb/'.$slide->image) }}" class="img img-responsive" />
          </div>
  </div>

  <div class="form-group{{ $errors->has('publish') ? ' has-error' : '' }}">
      {!! Form::label('publish', 'Publish', ['class' => 'col-sm-3 control-label']) !!}
      <div class="col-sm-9">
          {!! Form::select('publish', ['Y'=>'Yes', 'N'=>'No'], $slide->publish, ['class' => 'form-control']) !!}
          <small class="text-danger">{{ $errors->first('publish') }}</small>
      </div>
  </div>
@endif
<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
    {!! Form::label('image', 'Gambar Slideshow', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-9">
            {!! Form::file('image', ['class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('image') }}</small>
        </div>
</div>
