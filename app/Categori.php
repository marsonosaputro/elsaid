<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class Categori extends Model
{
    protected $fillable = ['title', 'slug'];

    function post()
    {
      return $this->hasMany('App\Post', 'categori_id');
    }
}
