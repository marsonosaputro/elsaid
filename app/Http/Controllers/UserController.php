<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Role;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Management User';
        $data['user'] = User::orderBy('id', 'desc')->paginate(10);
        $data['no'] = $data['user']->firstItem();
        return view('user.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Management User';
        return view ('user.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required|min:7',
        'email' => 'required|email|max:255|unique:users',
        'password' => 'required|min:8'
      ]);

      $user = User::create([
          'name' => $request['name'],
          'email' => $request['email'],
          'password' => bcrypt($request['password']),
      ]);
      $memberRole = Role::where('name', 'admin')->first();
      $user->attachRole($memberRole);

      Session::flash('flash_notification', [
              'level'=>'success',
              'message'=>'Berhasil menambahkan user <b>'.$request['name'].'</b>'
            ]);
      return redirect(route('user.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Management User';
        $data['user'] = User::findOrFail($id);
        return view('user.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'name' => 'required|min:7',
        'email' => 'required|email|max:255|unique:users,email,'.$id,
        'password' => 'sometimes|min:8'
      ]);
      $data = User::findOrFail($id);
      $user = $data->update([
          'name' => $request['name'],
          'email' => $request['email'],
          'password' => !empty($request['password']) ? bcrypt($request['password']) : $data->password,
      ]);
      Session::flash('flash_notification', [
              'level'=>'info',
              'message'=>'Berhasil mengubah user <b>'.$request['name'].'</b>'
            ]);
      return redirect(route('user.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user = User::findOrFail($id);
      Session::flash('flash_notification', [
              'level'=>'danger',
              'message'=>'Berhasil maghapus user <b>'.$user->name.'</b>'
            ]);
      $user->delete();
      return redirect(route('user.index'));
    }
}
