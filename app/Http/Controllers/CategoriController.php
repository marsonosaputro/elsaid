<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Categori;
use Session;

class CategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Daftar Kategori';
        $data['Kategori'] = Categori::orderBy('id', 'desc')->paginate(5);
        $data['no'] = $data['Kategori']->firstItem();
        return view('categori.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['page_title'] = 'Tambah Kategori';
      return view('categori.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['title'=>'required|unique:categoris,title']);
        $data = $request->all();
        $data['slug'] = str_slug($request['title']);
        Categori::create($data);
        Session::flash('flash_notification', [
                  'level'=>'success',
                  'message'=>'Berhasil menyimpan kategori <b>'.$data['title'].'</b>'
                ]);
        return redirect('admin/kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['page_title'] = 'Edit Kategori';
      $data['edit'] = Categori::find($id);
      return view('categori.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, ['title'=>'required|unique:categoris,title,'.$id]);
      $Kategori = Categori::find($id);
      $data = $request->all();
      $data['slug'] = str_slug($request['title']);
      $Kategori->update($data);
      Session::flash('flash_notification', [
                'level'=>'info',
                'message'=>'Berhasil mengubah kategori ke <b>'.$data['title'].'</b>'
              ]);
      return redirect('admin/kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Session::flash('flash_notification', [
                  'level'=>'danger',
                  'message'=>'Kategori <b>'.Categori::find($id)->title.'</b> berhasil dihapus'
                ]);
        Categori::find($id)->delete();
        return redirect('admin/kategori');
    }
}
