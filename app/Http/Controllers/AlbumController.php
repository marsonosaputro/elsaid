<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Album;
use Session;
use Image;
use File;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Management Album';
        $data['album'] = Album::orderBY('id', 'desc')->paginate(8);
        $data['no'] = $data['album']->firstItem();
        return view('album.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['page_title'] = 'Management Album';
      return view('album.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'album' => 'required',
          'gambar' => 'required'
        ]);

        if(!empty($request->file('gambar'))){
          $image = time().$request->file('gambar')->getClientOriginalName();
          $request->file('gambar')->move('images/album/', $image);
          $img = Image::make(public_path().'/images/album/'.$image)->crop(200,200);
          $img->save();
        }else{
          $image = '';
        }

        $data = $request->all();
        $data['gambar'] = $image;
        Album::create($data);
        Session::flash('flash_notification', [
                'level'=>'success',
                'message'=>'Berhasil menambahkan Album <b>'.$request['album'].'</b>'
              ]);
        return redirect(route('album.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Management Album';
        $data['album'] = Album::findOrFail($id);
        return view('album.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'album' => 'required',
      ]);

      $album = Album::findOrFail($id);
      if(!empty($request->file('gambar'))){
        $image = time().$request->file('gambar')->getClientOriginalName();
        $request->file('gambar')->move('images/album/', $image);
        $img = Image::make(public_path().'/images/album/'.$image)->crop(200,200);
        $img->save();
        File::delete('images/album/'.$album->gambar);
      }else{
        $image = $album->gambar;
      }

      $data = $request->all();
      $data['gambar'] = $image;
      $album->update($data);
      Session::flash('flash_notification', [
              'level'=>'info',
              'message'=>'Berhasil mengubah Album <b>'.$request['album'].'</b>'
            ]);
      return redirect(route('album.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $album = Album::findOrFail($id);
        File::delete('images/album/'.$album->gambar);
        Session::flash('flash_notification', [
                'level'=>'danger',
                'message'=>'Berhasil manghapus Album <b>'.$album->album.'</b>'
              ]);
        $album->delete();
        return redirect(route('album.index'));
    }
}
