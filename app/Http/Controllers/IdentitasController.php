<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Identitas;
use Session;

class IdentitasController extends Controller
{
    public function index()
    {
      $data['page_title'] = 'Identitas Website';
      $data['identitas'] = Identitas::where('id',1)->first();
      return view('identitas', $data);
    }

    public function update(Request $request, $id)
    {
      $identitas = Identitas::findOrFail($id);
      $data = $request->all();
      $identitas->update($data);
      Session::flash('flash_notification', [
                'level'=>'info',
                'message'=>'Identitas berhasil di update'
              ]);
      return redirect('admin/identitas');
    }
}
