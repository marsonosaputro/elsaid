<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Contact;
use Session;
use DB;

class ContactController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth', ['except'=>['store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Management Contact';
        $data['contact'] = Contact::orderBy('status', 'asc')->paginate(10);
        return view('contact.index', $data)->with('no', $data['contact']->firstItem());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required',
        'email' => 'required|email',
        'phone' => 'required|min:10|max:15',
        'subject' => 'required',
        'message' => 'required'
      ]);
      $data = $request->all();
      $data['status'] = '1';
      Contact::create($data);
      Session::flash('flash_notification', [
                'level'=>'success',
                'message'=>'Succes Send Your Message'
              ]);
      return redirect('contact');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['page'] = 'Show Contact';
        $data['contact'] = Contact::findOrFail($id);
        DB::table('contacts')->where('id', $id)->update(['status' => '2']);
        return view('contact.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Contact::findOrFail($id)->delete();
        return redirect('kontak');
    }
}
