<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Slideshow;
use App\Statis;
use App\Gallery;
use App\Album;
use App\Identitas;

class HomeController extends Controller
{
    public function index()
    {
        $data['page_title'] = 'Elsaid Kraton Mas';
        $data['slideshow'] = Slideshow::where('publish', 'Y')->take(3)->get();
        $data['welcome'] = Statis::where('id', '1')->first();
        $data['gallery'] = Gallery::where('album_id', '1')->take(4)->get();
        $data['produk'] = Gallery::where('album_id', '2')->take(8)->get();
        return view('frontend.home', $data);

    }

    public function about()
    {
        $data['page_title'] = 'about us';
        $data['about']  = Statis::where('id','2')->first();
        $data['visi'] = Statis::where('id','3')->first();
        $data['values'] = Statis::where('id','4')->first();
        $data['produk'] = Statis::where('id','5')->first();
        return view('frontend.about',$data);
    }

    public function carrier()
    {
      $data['page_title'] = 'Carrier';
      $data['carrier'] = Statis::where('id','6')->first();
      return view('frontend.carrier',$data);
    }

    public function contact()
    {
      $data['page_title'] = 'about us';
      $data['identitas'] = Identitas::where('id',1)->first();
      return view('frontend.contact', $data);
    }

    public function gallery($album_id=null)
    {
      $data['page_title'] = 'Gallery';
      if(!empty($album_id)){
        $data['gallery'] = Gallery::where('album_id', $album_id)->paginate(6);
      }else{
        $data['gallery'] = Gallery::orderBy('id', 'desc')->paginate(6);
      }
      $data['album'] = Album::orderBy('id', 'desc')->get();
      return view('frontend.gallery', $data)->with('no', $data['gallery']->firstItem());
    }
}
