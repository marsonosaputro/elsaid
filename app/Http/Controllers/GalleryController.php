<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Gallery;
use App\Album;
use Session;
use File;
use Image;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Management Gallery';
        $data['gallery'] = Gallery::orderBy('id', 'desc')->paginate(10);
        $data['no'] = $data['gallery']->firstItem();
        return view('gallery.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['page_title'] = 'Management Gallery';
      $data['album'] = Album::pluck('album', 'id');
      return view('gallery.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'judul' => 'required',
          'keterangan' => 'required',
          'gambar' => 'required'
        ]);

        if(!empty($request->file('gambar'))){
          $image = time().$request->file('gambar')->getClientOriginalName();
          $request->file('gambar')->move('images/gallery/', $image);
          Image::make(public_path().'/images/gallery/'.$image)->resize(720,550)->save();
          File::copy(public_path().'/images/gallery/'.$image, public_path().'/images/gallery/thumb/'.$image);
          Image::make(public_path().'/images/gallery/thumb/'.$image)->resize(270,183)->save();
        }else{
          $image = '';
        }

        $data = $request->all();
        $data['gambar'] = $image;
        Gallery::create($data);
        Session::flash('flash_notification', [
                'level'=>'success',
                'message'=>'Berhasil menambahkan gallery <b>'.$request['keterangan'].'</b>'
              ]);
        return redirect(route('gallery.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['page_title'] = 'Management Gallery';
      $data['gallery'] = Gallery::findOrFail($id);
      $data['album'] = Album::pluck('album', 'id');
      return view('gallery.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'judul' => 'required',
        'keterangan' => 'required',
      ]);

      $gallery = Gallery::findOrFail($id);
      if(!empty($request->file('gambar'))){
        $image = time().$request->file('gambar')->getClientOriginalName();
        $request->file('gambar')->move('images/gallery/', $image);
        Image::make(public_path().'/images/gallery/'.$image)->resize(720,550)->save();
        File::copy(public_path().'/images/gallery/'.$image, public_path().'/images/gallery/thumb/'.$image);
        Image::make(public_path().'/images/gallery/thumb/'.$image)->resize(270,183)->save();
        File::delete('images/gallery/'.$gallery->gambar);
        File::delete('images/gallery/thumb/'.$gallery->gambar);
      }else{
        $image = $gallery->gambar;
      }

      $data = $request->all();
      $data['gambar'] = $image;
      $gallery->update($data);
      Session::flash('flash_notification', [
              'level'=>'info',
              'message'=>'Berhasil mengubah gallery <b>'.$request['album'].'</b>'
            ]);
      return redirect(route('gallery.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $gallery = Gallery::findOrFail($id);
      File::delete('images/gallery/'.$gallery->gambar);
      File::delete('images/gallery/thumb/'.$gallery->gambar);
      Session::flash('flash_notification', [
              'level'=>'danger',
              'message'=>'Berhasil manghapus gallery <b>'.$gallery->keterangan.'</b>'
            ]);
      $gallery->delete();
      return redirect(route('gallery.index'));
    }
}
