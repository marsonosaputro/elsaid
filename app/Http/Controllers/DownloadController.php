<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Apfelbox\FileDownload\FileDownload;
use App\Download;
use Session;
use File;
use DB;

class DownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Download';
        $data['download'] = Download::orderBY('id', 'desc')->paginate(10);
        $data['no'] = $data['download']->firstItem();
        return view('download.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['page_title'] = 'Download';
      return view('download.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'judul' => 'required',
        'file' => 'required'
      ]);

      $file = time().$request->file('file')->getClientOriginalName();
      $request->file('file')->move('downloads/', $file);

      $data = $request->all();
      $data['file'] = $file;
      $data['hits'] = 0;
      Download::create($data);
      Session::flash('flash_notification', [
              'level'=>'success',
              'message'=>'Berhasil menambahkan file <b>'.$request['judul'].'</b>'
            ]);
      return redirect(route('download.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['page_title'] = 'Download';
      $data['download'] = Download::findOrFail($id);
      return view('download.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'judul' => 'required',
      ]);

      $download = Download::findOrFail($id);
      if(!empty($request->file('file'))){
        $file = time().$request->file('file')->getClientOriginalName();
        $request->file('file')->move('downloads/', $file);
        File::delete('downloads/'.$download->file);
      }else{
        $file = $download->file;
      }

      $data = $request->all();
      $data['file'] = $file;
      $data['hits'] = $download->hits;
      $download->update($data);
      Session::flash('flash_notification', [
              'level'=>'info',
              'message'=>'Berhasil mengubah file <b>'.$request['judul'].'</b>'
            ]);
      return redirect(route('download.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $download = Download::findOrFail($id);
      File::delete('downloads/'.$download->file);
      Session::flash('flash_notification', [
              'level'=>'danger',
              'message'=>'Berhasil manghapus file <b>'.$download->judul.'</b>'
            ]);
      $download->delete();
      return redirect(route('download.index'));
    }

    public function download($id)
    {
      $download = Download::find($id);
      $next = $download->hits + 1;
      DB::table('downloads')->where('id', $id)->update(['hits'=>$next]);
      $fileDownload = FileDownload::createFromFilePath(public_path().'/downloads/'.$download->file);
      $fileDownload->sendDownload($download->file);
      return redirect(route('download.index'));
    }
}
