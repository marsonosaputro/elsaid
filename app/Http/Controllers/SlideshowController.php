<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Slideshow;
use Session;
use Image;
use File;

class SlideshowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Management Slideshow';
        $data['slide'] = Slideshow::orderBY('id', 'desc')->paginate(5);
        return view('slideshow.index', $data)->with('no', 1);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $data['page_title'] = 'Management Slideshow';
      return view('slideshow.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['judul'=>'required',
                                   'keterangan'=>'sometimes',
                                   'image'=>'required']);

        if(!empty($request->file('image'))){
          $image = time().$request->file('image')->getClientOriginalName();
          $request->file('image')->move('images/slideshow/', $image);
          $img = Image::make(public_path().'/images/slideshow/'.$image)->resize(1920,1125)->save();

          File::copy(public_path().'/images/slideshow/'.$image, public_path().'/images/slideshow/thumb/'.$image);
          $img2 = Image::make(public_path().'/images/slideshow/thumb/'.$image)->resize(200,100)->save();

        }else{
          $image = '';
        }

        $data = $request->all();
        $data['image'] = $image;
        $data['publish'] = 'Y';
        Slideshow::create($data);

        Session::flash('flash_notification', [
                'level'=>'success',
                'message'=>'Berhasil menyimpan slideshow <b>'.$request['judul'].'</b>'
              ]);
        return redirect(route('slideshow.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Management Slideshow';
        $data['slide'] = Slideshow::findOrFail($id);
        return view('slideshow.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, ['judul'=>'required',
                                 'keterangan'=>'sometimes',
                                 'image'=>'sometimes']);
      $slide = Slideshow::findOrFail($id);
      if(!empty($request->file('image'))){
        File::delete(public_path().'/images/slideshow/'.$slide->image);
        File::delete(public_path().'/images/slideshow/thumb/'.$slide->image);

        $image = time().$request->file('image')->getClientOriginalName();
        $request->file('image')->move('images/slideshow/', $image);
        Image::make(public_path().'/images/slideshow/'.$image)->resize(1920,1125)->save();

        File::copy(public_path().'/images/slideshow/'.$image, public_path().'/images/slideshow/thumb/'.$image);
        Image::make(public_path().'/images/slideshow/thumb/'.$image)->resize(200,100)->save();
      }else{
        $image = $slide->image;
      }

      $data = $request->all();
      $data['image'] = $image;
      $slide->update($data);

      Session::flash('flash_notification', [
              'level'=>'info',
              'message'=>'Berhasil mengubah slideshow <b>'.$request['judul'].'</b>'
            ]);
      return redirect(route('slideshow.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slideshow::findOrFail($id);
        File::delete(public_path().'/images/slideshow/'.$slide->image);
        File::delete(public_path().'/images/slideshow/thumb/'.$slide->image);
        Session::flash('flash_notification', [
                'level'=>'danger',
                'message'=>'Berhasil menghapus slideshow <b>'.$slide['judul'].'</b>'
              ]);
        $slide->delete();
        return redirect(route('slideshow.index'));
    }
}
