<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\SimpanBeritaRequest;
use App\Post;
use App\Categori;
use Image;
use Session;
use DB;
use Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['page_title'] = 'Management Berita';
        $data['berita'] = Post::orderBy('id', 'desc')->paginate(10);
        $data['no'] = $data['berita']->firstItem();
        return view('berita.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['page_title'] = 'Tambah Berita';
        $data['kategori'] = Categori::pluck('title', 'id');
        return view('berita.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SimpanBeritaRequest $request)
    {
      if(!empty($request->file('image'))){
          $image = time().$request->file('image')->getClientOriginalName();
          $request->file('image')->move('images/post/', $image);
          $img = Image::make(public_path().'/images/post/'.$image)->crop(450,578);
          $img->save();
      }else{
          $image = '';
      }

      $post = $request->all();
      $post['slug'] = str_slug($request['judul'], '-');
      $post['image'] = $image;
      $post['penulis'] = Auth::user()->name;
      $post['hits'] = 0;
      Session::flash('flash_notification', [
                'level'=>'success',
                'message'=>'Berhasil menyimpan berita <b>'.$request['judul'].'</b>'
              ]);
      Post::create($post);
      return redirect('/admin/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['page_title'] = 'Ubah berita';
        $data['kategori'] = Categori::pluck('title', 'id');
        $data['berita'] = Post::findOrFail($id);
        return view('berita.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SimpanBeritaRequest $request, $id)
    {
      $berita = Post::findOrFail($id);
      if(!empty($request->file('image'))){
          $image = time().$request->file('image')->getClientOriginalName();
          $request->file('image')->move('images/post/', $image);
          $img = Image::make(public_path().'/images/post/'.$image)->crop(450,578);
          $img->save();
      }else{
          $image = $berita->image;
      }

      $post = $request->all();
      $post['slug'] = str_slug($request['judul'], '-');
      $post['image'] = $image;
      $post['penulis'] = Auth::user()->name;
      $post['hits'] = $berita->hits;
      Session::flash('flash_notification', [
                'level'=>'info',
                'message'=>'Berhasil mengubah berita <b>'.$request['judul'].'</b>'
              ]);
      $berita->update($post);
      return redirect('/admin/post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Post::findOrFail($id);
        Session::flash('flash_notification', [
                  'level'=>'danger',
                  'message'=>'Berhasil menghapus berita <b>'.$berita['judul'].'</b>'
                ]);
        $berita->delete();
        return redirect('/admin/post');
    }
}
