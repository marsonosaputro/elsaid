<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Gallery;

class Album extends Model
{
    protected $fillable = ['album', 'gambar'];

    public function gallery()
    {
      return $this->hasMany('App\Gallery', 'id');
    }
}
