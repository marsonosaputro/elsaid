<?php

namespace App;
use App\Album;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = ['album_id', 'judul', 'gambar', 'keterangan'];

    public function album()
    {
      return $this->belongsTo('App\Album', 'id');
    }
}
