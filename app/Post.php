<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Categori;

class Post extends Model
{
    protected $fillable = ['categori_id', 'judul', 'slug', 'content', 'image', 'penulis', 'hits'];

    public function categori()
    {
      return $this->belongsTo('App\Categori', 'id');
    }
}
